import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from '@/router'
import store from '@/store'

import '@/assets/global.css'

import BaseBtn from '@/components/base/Btn'
import BaseCard from '@/components/base/Card'
import BaseSubheading from '@/components/base/Subheading'

Vue.config.productionTip = false

Vue.component(BaseBtn.name, BaseBtn)
Vue.component(BaseCard.name, BaseCard)
Vue.component(BaseSubheading.name, BaseSubheading)

import Request from '@/plugins/request';
Vue.prototype.$axios = Request

import VueLogger from 'vuejs-logger'
const isProduction = process.env.NODE_ENV === 'production'
const options = {
  isEnabled: true,
  logLevel: isProduction ? 'error' : 'info',
  stringifyArguments: false,
  showLogLevel: true,
  showMethodName: true,
  separator: '|',
  showConsoleColors: true
}

Vue.use(VueLogger, options)

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
