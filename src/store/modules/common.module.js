const state = {
    articles: require('@/data/articles.json'),
    drawer: true,
    token: null
}
const getters = {
    getStateDrawer: state => {
        return state.drawer;
    }
}
const mutations = {
    setDrawer(state, payload) {
        state.drawer = payload
    },
    toggleDrawer(state) {
        state.drawer = !state.drawer
    },
    setToken(state, payload) {
        state.token = payload
        sessionStorage.setItem('userToken', payload)
    }
}
const actions = {}

export default {
    state,
    getters,
    mutations,
    actions
}