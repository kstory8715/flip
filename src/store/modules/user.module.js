import request from '../../plugins/request';

const state = {
    user: null
}
const getters = {
    getUser: state => {
        return state.user;
    }
}
const mutations = {
    setUser(state, payload) {
        state.user = payload
    }
}
const actions = {
    getUserDispatch({ commit }, { vm }) {
        const acct_id = sessionStorage.getItem('acct_id');
        return request.get(`/v1/user/accounts/${acct_id}`)
            .then(({ data }) => {
                vm.$log.info('getUserDispatch | ', data)
                commit('setUser', data)
            })
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}