import axios from 'axios';
import store from '@/store'

// const isDebug = process.env.VUE_APP_DEBUG === "true";
const service = axios.create({
    baseURL: 'http://192.168.110.26:31019',
    timeout: 3000
});

function uuid() {
    const cry = window.crypto || window.msCrypto;
    return ([1e7] + 1e3 + 4e3 + 8e3 + 1e11).replace(/[018]/g, c =>
        (
            c ^
            (cry.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
        ).toString(16)
    );
}

// request interceptor
service.interceptors.request.use(config => {
    config.headers["X-CloudPC-Request-Poc"] = "POCUSER";
    config.headers["X-CloudPC-Request-ID"] = uuid();
    config.headers["Accept-Language"] = 'A001KO'
    let userToken = sessionStorage.getItem('userToken');
    if (userToken) {
      config.headers["authorization"] = userToken;
    }
    return config;
});

service.interceptors.response.use(res => {
    const token = res.headers.authorization;
    if (token) {
        store.commit("setToken", token);
    }
    res.data = res.data.data || res.data;
    return res
})

export default service;
