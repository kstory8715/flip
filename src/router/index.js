import Vue from 'vue'
import Router from 'vue-router'
import Layout from '../views/layout/Layout'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: require('@/views/redirect/index').default
    },
    {
      path: '/login',
      component: require('@/views/login/Login').default,
      hidden: true
    },
    {
      path: '',
      component: Layout,
      redirect: 'dashboard',
      children: [
        {
          path: 'dashboard',
          component: require('@/views/Home').default,
          meta: {
            id: 'A01',
            tenantMenuId: 'T01',
            title: 'button.navbar.dashboard',
            icon: 'dashboard',
            noCache: true,
            isNotHeader: true
          }
        }
      ]
    },
  ]
})
