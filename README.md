# User Portal

<p align="center">
  <img src="https://img1.daumcdn.net/thumb/R800x0/?scode=mtistory2&fname=https%3A%2F%2Fk.kakaocdn.net%2Fdn%2Fnprmt%2Fbtqu0463kbN%2FGso0UtWFz6UY0X1DLcOSnk%2Fimg.jpg" />
</p>

### Project clone

```
git clone git@gitlab.com:kstory8715/flip.git
```

### Project install

```
yarn
```

### App start

```
yarn run serve
```

### App build

```
yarn run build
```
