module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  // vue.config.js
  chainWebpack: (config) => {
    config.plugins.delete('prefetch')
  }
}